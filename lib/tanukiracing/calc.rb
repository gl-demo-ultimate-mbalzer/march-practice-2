 #define a calculator class for other functions to use
 class Calculator
  def initialize
    @memory = 0
  end
  
  def add(num)
    @memory += num
  end

  def subtract(num)
    @memory -= num
  end

  def multiply(num)
    @memory *= num
  end

  def divide(num)
    @memory /= num if num != 0
  end

  def memory
    @memory
  end
end
